﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Send
{
    class Program
    {
 
        public class Person
        {

            public string Name { get; set; }
            public string LastName { get; set; }
            public int Age { get; set; }
        }

        static void Main(string[] args)
        {

            //thread sleep send message automatic
            while (true)
            {
                var factory = new ConnectionFactory() { HostName = "localhost" };
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: "Object",
                                         durable: false,
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: null);

                    //get random name
                    var names = new string[]
                     {
                        "ali","hassan","mori","hamed","fatemeh","zahra","asgar","hashem","molavi","eshagh",
                        "ziba","behzad","parham","meysam","sajad","vahid","bahar","babak","shima","shiba","sina",
                        "arash","ghazanfar","hossien","mohammad","sohiel","sadegh","abas","amir","reza","naghi"
                    };
                    var randomName = new Random();
                    var idname = randomName.Next(names.Length);
                    string name = names[idname];


                    //get random lastname
                    var LastNames = new string[]
                     {
                        "bayat","abasi","gharadagh","safarzade","moradi","bayat","ahadi","khomini","sarei","jahangir",
                        "donaie","fathi","yasini","farsi","ziadlo","zolfaghari","mosavi","asheri","rezaie","inu","saie",
                        "intelijence","lopo","pourBahrami","khaki","latifi","hasani","babaie","kashani","mohamadi","allahyari"
                    };
                    var randomLastname = new Random();
                    var idlastname = randomLastname.Next(LastNames.Length);
                    string lastName = LastNames[idlastname];


                    //get random age
                    Random rndage = new Random();
                    int age = rndage.Next(15, 70);


                    Person p = new Person();
                    p.Name = name;
                    p.LastName = lastName;
                    p.Age = age;

                    byte[] byteArray = ObjectToByteArray(p);

                    channel.BasicPublish("", routingKey: "Object", basicProperties: null, body: byteArray);
                    Console.WriteLine(" [x] Sent");
                }
            
                Thread.Sleep(5000);
            }

        }


        //obejct to array
        public static byte[] ObjectToByteArray(Person p)
        {
            using (MemoryStream m = new MemoryStream())
            {
                using (BinaryWriter writer = new BinaryWriter(m))
                {
                    writer.Write(p.Name);
                    writer.Write(p.LastName);
                    writer.Write(p.Age);
                }
                return m.ToArray();
            }
        }
    }
}

